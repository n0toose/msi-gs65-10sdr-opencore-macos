# OpenCore macOS configurations for the MSI GS65 Stealth 10SDR

- macOS version: Big Sur (11.x)

|Function / Hardware|Status|
|-------------------|------|
|dGPU NVIDIA GTX1660Ti Acceleration|Not Working|
|Audio|Not Working|
|iGPU UHD630 Acceleration|Working|
|Laptop Keyboard|Working|
|Laptop Trackpad|Working|
|Laptop Headphones Jack|Working|
|Built-in Speakers|Not working|
|Built-in Mic|Not working|
|Hotkeys for audio, brightness, webcam, touchpad|Working|
|USB 3.x|Working|
|Screen brightness|Not working|
|Built-in Wifi|Working|
|Built-in Bluetooth|Working|
|USB type C|Working|
|Ethernet|Working|
|Full-Disk Encryption|Working|
